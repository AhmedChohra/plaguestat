<?php

namespace App\Entity;

use App\Repository\StatisticRepository;
use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass=StatisticRepository::class)
 */
class Statistic
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $contamined;

    /**
     * @ORM\Column(type="integer")
     */
    private $healed;

    /**
     * @ORM\Column(type="integer")
     */
    private $zombified;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="statistics")
     * @ORM\JoinColumn(nullable=false)
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity=Virus::class, inversedBy="statistics")
     * @ORM\JoinColumn(nullable=false)
     */
    private $virus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContamined(): ?int
    {
        return $this->contamined;
    }

    public function setContamined(int $contamined): self
    {
        $this->contamined = $contamined;

        return $this;
    }

    public function getHealed(): ?int
    {
        return $this->healed;
    }

    public function setHealed(int $healed): self
    {
        $this->healed = $healed;

        return $this;
    }

    public function getZombified(): ?int
    {
        return $this->zombified;
    }

    public function setZombified(int $zombified): self
    {
        $this->zombified = $zombified;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getVirus(): ?Virus
    {
        return $this->virus;
    }

    public function setVirus(?Virus $virus): self
    {
        $this->virus = $virus;

        return $this;
    }
}
