<?php

namespace App\DataFixtures;

use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CountryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        function createCountry($name, ObjectManager $manager){
            $country = new Country($name);
            $country->setName( $name );
            $manager->persist($country);
    }
        createCountry("France", $manager);
        createCountry("Italie", $manager);
        createCountry("Allemagne", $manager);
        createCountry("Espagne", $manager);
        createCountry("Royaume-Uni", $manager);

        $manager->flush();
    }
}
