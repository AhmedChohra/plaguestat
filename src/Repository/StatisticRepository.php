<?php

namespace App\Repository;

use App\Entity\Statistic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Statistic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Statistic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Statistic[]    findAll()
 * @method Statistic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatisticRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Statistic::class);
    }


    public function findLastByCountry()
    {
        return $this->createQueryBuilder('s')
            ->groupBy('s.country')
            ->orderBy('s.createdAt', 'Desc')
            ->getQuery()
            ->getResult()
        ;
    }


    public function findByCountry($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.country = :val')
            ->setParameter('val', $value)
            ->orderBy('s.createdAt', 'Desc')
            ->getQuery()
            ->getResult()
        ;
    }


    public function findByCountryAndVirus($value1, $value2)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.country = :val1')
            ->setParameter('val1', $value1)
            ->andWhere('s.virus = :val2')
            ->setParameter('val2', $value2)
            ->getQuery()
            ->getResult()
            ;
    }

    public function countByContamined($value1)
    {
        return $this->createQueryBuilder('s')
            ->select('SUM(s.contamined)')
            ->andWhere('s.country = :val')
            ->setParameter('val', $value1)
            ->groupBy('s.country')
            ->getQuery()
            ->getResult()
            ;
    }

    public function countByHealed($value1)
    {
        return $this->createQueryBuilder('s')
            ->select('SUM(s.healed)')
            ->andWhere('s.country = :val')
            ->setParameter('val', $value1)
            ->groupBy('s.country')
            ->getQuery()
            ->getResult()
            ;
    }

    public function countByZombified($value1)
    {
        return $this->createQueryBuilder('s')
            ->select('SUM(s.zombified)')
            ->andWhere('s.country = :val')
            ->setParameter('val', $value1)
            ->groupBy('s.country')
            ->getQuery()
            ->getResult()
            ;
    }

    public function Contamined()
    {
        return $this->createQueryBuilder('s')
            ->select('SUM(s.contamined)')
            ->getQuery()
            ->getResult()
            ;
    }

    public function Healed()
    {
        return $this->createQueryBuilder('s')
            ->select('SUM(s.healed)')
            ->getQuery()
            ->getResult()
            ;
    }

    public function Zombified()
    {
        return $this->createQueryBuilder('s')
            ->select('SUM(s.zombified)')
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Statistic
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
