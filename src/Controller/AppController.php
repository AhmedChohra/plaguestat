<?php

namespace App\Controller;

use App\Repository\CountryRepository;
use App\Repository\StatisticRepository;

use http\Env\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{
    /**
     * @Route("/", name="app_index")
     */
    public function index(StatisticRepository $statisticRepository)
    {
        $contamined = $statisticRepository->Contamined();
        $healed = $statisticRepository->Healed();
        $zombified = $statisticRepository->Zombified();

        return $this->render('app/index.html.twig', [
            'statistics' => $statisticRepository->findLastByCountry(),
            'contamined' => $contamined[0][1],
            'healed' => $healed[0][1],
            'zombified' => $zombified[0][1]
        ]);
    }
}
