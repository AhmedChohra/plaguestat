<?php

namespace App\Controller;

use App\Entity\Virus;
use App\Form\VirusType;
use App\Repository\VirusRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/virus")
 */
class VirusController extends AbstractController
{
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/", name="virus_index", methods={"GET"})
     */
    public function index(VirusRepository $virusRepository): Response
    {
        return $this->render('virus/index.html.twig', [
            'viri' => $virusRepository->findAll(),
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/new", name="virus_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $viru = new Virus();
        $form = $this->createForm(VirusType::class, $viru);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($viru);
            $entityManager->flush();

            return $this->redirectToRoute('virus_index');
        }

        return $this->render('virus/new.html.twig', [
            'viru' => $viru,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/{id}", name="virus_show", methods={"GET"})
     */
    public function show(Virus $viru): Response
    {
        return $this->render('virus/show.html.twig', [
            'viru' => $viru,
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/{id}/edit", name="virus_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Virus $viru): Response
    {
        $form = $this->createForm(VirusType::class, $viru);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('virus_index');
        }

        return $this->render('virus/edit.html.twig', [
            'viru' => $viru,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/{id}", name="virus_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Virus $viru): Response
    {
        if ($this->isCsrfTokenValid('delete'.$viru->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($viru);
            $entityManager->flush();
        }

        return $this->redirectToRoute('virus_index');
    }
}
