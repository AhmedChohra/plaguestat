<?php

namespace App\Form;

use App\Entity\Country;
use App\Entity\Statistic;
use App\Entity\Virus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StatisticType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contamined', NumberType::class, ['label' => 'Contaminés'])
            ->add('healed', NumberType::class, ['label' => 'Guéris'])
            ->add('zombified', NumberType::class, ['label' => 'Zombifiés'])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'choice_label' => 'name',
                'label' => 'pays'
            ])
            ->add('virus', EntityType::class, [
                'class' => Virus::class,
                'choice_label' => 'codeName'
            ])
            ->add('createdAt', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date de création'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Statistic::class,
        ]);
    }
}
