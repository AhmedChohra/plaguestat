<?php

namespace App\Form;

use App\Entity\Virus;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class VirusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codeName', TextType::class, [
                'label' => 'Zombifiés'

            ])
            ->add('dangerousness', TextType::class, [
                'label' => 'Dangerosité',
                'constraints' => [
                    new LessThanOrEqual([
                        'value' => 30,
                        'message' => "La valeur {{ value }} saisie est supérieure à la note maximale de {{ compared_value }}."
                    ]),
                    new GreaterThanOrEqual([
                        'value' => 1,
                        'message' => "La valeur {{ value }} saisie est inférieure à la note minimale de {{ compared_value }}."
                    ]),

            ]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Virus::class,
        ]);
    }
}
